# Evaluation of the bibliography of the Fraunhofer Gesellschaft
Source: Fraunhofer-Publica (https://publica.fraunhofer.de/home)

## Results
### fraunhofer_journals.csv
Ranked list of all **journals** in which the Fraunhofer Gesellschaft has published

### fraunhofer_oa_journals.csv
Ranked list of all **open access journals** in which the Fraunhofer Gesellschaft has published

### fraunhofer_oa_publishers.csv
Ranked list of all **open access publishers** in which the Fraunhofer Gesellschaft has published

### fraunhofer_conferences.csv
Ranked list of all **conferences** to which Fraunhofer Institutes have contributed (conference paper, presentation oder conference proceeding)


## Tools
### Fraunhofer.ipynb
Evaluation script

### harvesting_script.ipynb
Harvesting script

### doaj/doaj_dump.csv
Contains all ISSN listed in DOAJ (Open Acces journals).
Source: https://doaj.org/docs/public-data-dump/

![NFDI4ing](logos/Image20231213185704.png)
